import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class Ajouter{
	public static String[][] addTableau(String[][] tableau,Scanner sc,int taille){
		String[][] tableauAdd = MethodeProjetBanque.ajouterAncienTableau(tableau,taille);
		if(taille==3){
			tableauAdd = tableauAddAgence(tableau,tableauAdd, sc);
		}
		if(taille==5){
			tableauAdd = tableauAddClient(tableau,tableauAdd, sc);
		}
		if(taille==6){
			tableauAdd = tableauAddCompteBancaire(tableau,tableauAdd, sc);
		}
		return tableauAdd;
	}
	public static String[][] tableauAddAgence(String[][] tableau,String[][] addTableau,Scanner sc){
		String[][] tableauAgence = addTableau;
		String message = "Saisir le code agence (il doit se composer de trois chiffres) : ";
		tableauAgence[addTableau.length-1][0] = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{3}",message,tableau,false);
		message = "Saisir le nom de l'agence : ";
		tableauAgence[addTableau.length-1][1] = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z\\-\\s]{3,50}",message);
		tableauAgence[addTableau.length-1][1] = tableauAgence[addTableau.length-1][1].substring(0,1).toUpperCase()+tableauAgence[addTableau.length-1][1].substring(1).toLowerCase();
		message = "Saisir l'adresse de l'agence : ";
		tableauAgence[addTableau.length-1][2] = MethodeProjetBanque.saisieTableauVerifie(sc,"[\\w\\s]{5,150}",message);
		MethodeProjetBanque.afficherTableau(tableauAgence);
		return tableauAgence;
	}
	public static String[][] tableauAddClient(String[][] tableau,String[][] addTableau,Scanner sc){
		String[][] tableauClient = addTableau;
		String message = "Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : )";
		tableauClient[addTableau.length-1][0] = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,tableau,false).toUpperCase();
		message = "Saisir le nom du client : ";
		tableauClient[addTableau.length-1][1] = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z]{3,30}",message).toUpperCase();
		message = "Saisir le prénom du client : ";
		tableauClient[addTableau.length-1][2] = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z]{1,20}[a-zA-Z\\-]{0,1}[a-zA-Z]{1,20}",message);
		tableauClient[addTableau.length-1][2] = tableauClient[addTableau.length-1][2].substring(0,1).toUpperCase()+tableauClient[addTableau.length-1][2].substring(1).toLowerCase();
		message = "Saisir la date de naissance du client (jjmmaaaa) : ";
		tableauClient[addTableau.length-1][3] = MethodeProjetBanque.ageVerifie(sc,message);
		message = "Saisir l'adresse email du client : ";
		tableauClient[addTableau.length-1][4] = MethodeProjetBanque.saisieTableauVerifie(sc,"\\w{3,20}\\.{0,1}\\w{3,20}\\@\\w{2,15}\\.[a-z]{2,7}",message);
		MethodeProjetBanque.afficherTableau(tableauClient);
		return tableauClient;
	}
	public static String[][] tableauAddCompteBancaire(String[][] tableau,String[][] addTableau,Scanner sc){
		String[][] tableauCompteBancaire = new String[0][0];
		if(MethodeProjetBanque.AGENCES.length==0||MethodeProjetBanque.AGENCES[0].length==0){
			System.out.println("Erreur, il est impossible de créer un compte bancaire sans agence !");
		}else if(MethodeProjetBanque.CLIENTS.length==0||MethodeProjetBanque.CLIENTS[0].length==0){
			System.out.println("Erreur, il est impossible de créer un compte bancaire sans client !");
		}else{
			tableauCompteBancaire = addTableau;
			String message = "Saisir le numéro de compte du compte bancaire (il se compose de 11 chiffres) : ";
			tableauCompteBancaire[addTableau.length-1][0] = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{11}",message,tableau,false);
			message = "Saisir le code agence (il doit se composer de trois chiffres) : ";
			tableauCompteBancaire[addTableau.length-1][1] = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{3}",message,MethodeProjetBanque.AGENCES,true);
			message = "Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : ";
			tableauCompteBancaire[addTableau.length-1][2] = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,MethodeProjetBanque.CLIENTS,true).toUpperCase();
			message = "Saisir la touche associée aux types de compte bancaire parmis\ncompte courrant : \"c\"\nlivret A : \"a\"\nplan d'épargne logement : \"e\"";
			tableauCompteBancaire[addTableau.length-1][3] = MethodeProjetBanque.typeCompteVerifie(sc,message);
			message = "Saisir le solde du compte bancaire : ";
			tableauCompteBancaire[addTableau.length-1][4] = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{1,20}",message);
			message = "Saisir les options du découvert : \"o\" pour autorisé\n\"n\" pour reffusé";
			tableauCompteBancaire[addTableau.length-1][5] = MethodeProjetBanque.optionDecouvertAutorise(sc,message);
		}
		
		MethodeProjetBanque.afficherTableau(tableauCompteBancaire);
		return tableauCompteBancaire;
	}
}