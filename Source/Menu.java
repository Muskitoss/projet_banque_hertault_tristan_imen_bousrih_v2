import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class Menu{
	public static void afficherMenu(){
		System.out.println("Menu: ");
		System.out.println();
		System.out.println("1- Creer une agence");
		System.out.println("2- Creer un client");
		System.out.println("3- Creer un compte bancaire");
		System.out.println("4- Modifier une agence");
		System.out.println("5- Modifier un client");
		System.out.println("6- Modifier un compte bancaire");
		System.out.println("7- Supprimer une agence");
		System.out.println("8- Supprimer un client");
		System.out.println("9- Supprimer un compte bancaire");
		System.out.println("10- Afficher la liste de tous les agences");
		System.out.println("11- Afficher la liste de tous les clients");
		System.out.println("12- Afficher la liste de tous les comptes");
		System.out.println("13- Recherche de compte (numero de compte)");
		System.out.println("14- Recherche de client (Nom, Numero de compte, identifiant de client)");
		System.out.println("15- Afficher la liste des comptes d’un client (identifiant client)");
		System.out.println("16- Imprimer les infos client (identifiant client)");
		System.out.println("17- Quitter le programme ");
		System.out.println();
		System.out.println("Saisir le numéro qui correspond au menu souhaité :\n");
	}
	public static String saisieMenu(Scanner sc){
		String saisie;
		boolean regex;
		do{
			saisie = sc.nextLine().trim();
			regex = saisie.matches("\\d{1,2}");
			if(!regex){
				System.out.println("Saisie invalide !");
			}else{
				if(Integer.valueOf(saisie)>17||Integer.valueOf(saisie)<1){
					regex = false;
					System.out.println("Saisie invalide !");
				}
			}
		}while(!regex);
		return saisie;
	}
	public static boolean identificationPassword(){
		Scanner sc = new Scanner(System.in);
		int login = 1234;
		int saisieLogin;
		int saisiePassword; 
		int password = 0000;
		for(int i=1;i<4;i++){
			System.out.print("veuillez entre votre identifiant:");
			saisieLogin = Integer.parseInt(sc.nextLine());
			
			if(saisieLogin==login){
				System.out.print("veuillez entre votre mots de passe:");
				saisiePassword = Integer.parseInt(sc.nextLine());
				if(saisieLogin==login&&saisiePassword == password){
					return true; 
				}else{
				System.out.println("Votre mots de passe est incorrect"+"\n"+ " Attension il vous reste "+(3-i)+" tentative" );
				}
			}else{	
				System.out.println("Votre identifiant est incorrect"+"\n"+ " Attension il vous reste "+(3-i)+" tentative" );
			}
		}
		return false;
	}
	public static void quitterProgramme(Scanner sc){
		boolean confirmation;
		System.out.print("Souhaitez-vous quitter le programme ? (oui pour confirmer) : ");
		String quitter = sc.nextLine().toLowerCase().trim();
		if (quitter.equals("oui")) {
		  System.out.println("Au revoir !");
		  confirmation = true;
		} else {
			System.out.println("vous etes toujours connecte");
			confirmation = false;
		}
		if(confirmation){
			System.exit(0);
		}
	}
}