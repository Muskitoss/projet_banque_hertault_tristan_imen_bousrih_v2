import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class Modifier{
	public static String[][] modifierTableau(String[][] tableau,Scanner sc,int taille){
		String[][] tableauModifie = tableau;
		if(taille==3){
			tableauModifie = tableauModifier(tableauModifie, sc);
		}
		if(taille==5){
			tableauModifie = tableauModifier(tableauModifie, sc);
		}
		if(taille==6){
			tableauModifie = tableauModifier(tableau, sc);
		}
		return tableauModifie;
	}
	public static String[][] tableauModifier(String[][] tableau,Scanner sc){
		String[][] tableauModifie = tableau;
		if(tableau.length==0||tableau[0].length==0){
			System.out.println("Erreur, il n'y a pas d'éléments à modifier !");
		}else{
			MethodeProjetBanque.afficherTableau(tableau);
			String message;
			String codeElement = "";
			if(tableau[0].length==3){
				message = "Saisir le code agence de l'élément à modifier (il doit se composer de trois chiffres) : ";
				codeElement = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{3}",message,tableau,true);
			}
			if(tableau[0].length==5){
				message = "Saisir l'id du client à modifier (il doit se composer de deux lettres en majuscules et de six chiffres) : )";
				codeElement = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,tableau,true).toUpperCase();
			}
			if(tableau[0].length==6){
				message = "Saisir le numéro de compte du compte bancaire à modifier(il se compose de 11 chiffres) : ";
				codeElement = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{11}",message,tableau,true);
			}
			MethodeProjetBanque.afficherTableau(tableau);
			message = "Saisir l'index de l'élément à modifier parmis les éléments ci-dessus\n(0,1,2,3,4 ou 5 selon le nombres d'éléments de l'option selectionnée, le 0 n'étant pas modifiable il est innutile de le saisir) :";
			String saisieIndex = MethodeProjetBanque.saisieIndexValide(tableau,message,sc);
			String nouvelElement = MethodeProjetBanque.saisieElementValide(tableau,saisieIndex,message,sc);
			tableauModifie = MethodeProjetBanque.tableauModifie(tableau,codeElement,saisieIndex,nouvelElement);
			MethodeProjetBanque.afficherTableau(tableau);
		}
		return tableauModifie;
	}
}