import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class Supprimer{
	public static String[][] supprimerTableau(String[][] tableau,Scanner sc,int taille){
		String[][] tableauSupp = MethodeProjetBanque.ajouterAncienTableau(tableau,taille);
		if(taille==3){
			tableauSupp = tableauSuppAgence(tableau, sc);
		}
		if(taille==5){
			tableauSupp = tableauSuppClient(tableau, sc);
		}
		if(taille==6){
			tableauSupp = tableauSuppCompteBancaire(tableau, sc);
		}
		return tableauSupp;
	}
	public static String[][] tableauSuppAgence(String[][] ancienTableau,Scanner sc){
		String[][] nouveauTableau = ancienTableau;
		if(ancienTableau.length==0||ancienTableau[0].length==0){
			System.out.println("Erreur, il n'y a pas d'éléments à supprimer !");
		}else{
			MethodeProjetBanque.afficherTableau(ancienTableau);
			String message;
			String codeElement = "";
			message = "Saisir le code agence de l'élément à supprimer (il doit se composer de trois chiffres) : ";
			codeElement = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{3}",message,ancienTableau,true);
			if(codeElement!="099"){
				nouveauTableau = MethodeProjetBanque.tableauSupprime(ancienTableau,codeElement,0);
				MethodeProjetBanque.COMPTES = MethodeProjetBanque.tableauModifie(MethodeProjetBanque.COMPTES,codeElement,"1","099",1);
				MethodeProjetBanque.afficherTableau(nouveauTableau);
			}else{
				System.out.println("Erreur, il est impossible de supprimer l'agence générique.");
			}
			
		}
		return nouveauTableau;
	}
	public static String[][] tableauSuppClient(String[][] ancienTableau,Scanner sc){
		String[][] nouveauTableau = ancienTableau;
		if(ancienTableau.length==0||ancienTableau[0].length==0){
			System.out.println("Erreur, il n'y a pas d'éléments à supprimer !");
		}else{
			MethodeProjetBanque.afficherTableau(ancienTableau);
			String message;
			String codeElement = "";
			message = "Saisir l'id du client à supprimer (il doit se composer de deux lettres en majuscules et de six chiffres) : ";
			codeElement = MethodeProjetBanque.saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,ancienTableau,true).toUpperCase();
			nouveauTableau = MethodeProjetBanque.tableauSupprime(ancienTableau,codeElement,0);
			MethodeProjetBanque.COMPTES = MethodeProjetBanque.tableauSupprime(MethodeProjetBanque.COMPTES,codeElement,2);
			MethodeProjetBanque.afficherTableau(nouveauTableau);
		}
		return nouveauTableau;
	}
	public static String[][] tableauSuppCompteBancaire(String[][] ancienTableau,Scanner sc){
		String[][] nouveauTableau = ancienTableau;
		if(ancienTableau.length==0||ancienTableau[0].length==0){
			System.out.println("Erreur, il n'y a pas d'éléments à supprimer !");
		}else{
			MethodeProjetBanque.afficherTableau(ancienTableau);
			String message;
			String codeElement = "";
			message = "Saisir le numéro de compte du compte bancaire à modifier(il se compose de 11 chiffres) : ";
			codeElement = MethodeProjetBanque.saisieTableauVerifie(sc,"\\d{11}",message,ancienTableau,true);
			nouveauTableau = MethodeProjetBanque.tableauSupprime(ancienTableau,codeElement,0);
			MethodeProjetBanque.afficherTableau(nouveauTableau);
		}
		return nouveauTableau;
	}
}