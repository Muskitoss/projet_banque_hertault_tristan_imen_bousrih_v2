import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class MainProjetBanque{
	public static void main(String[] args){
		boolean connected = Menu.identificationPassword();
		Scanner sc = new Scanner(System.in);
		String saisie;
		String message;
		MethodeProjetBanque.AGENCES = GestionCSV.lectureFichiersCSV("Agences.csv",MethodeProjetBanque.AGENCES,3);
		MethodeProjetBanque.CLIENTS = GestionCSV.lectureFichiersCSV("Clients.csv",MethodeProjetBanque.CLIENTS,5);
		MethodeProjetBanque.COMPTES = GestionCSV.lectureFichiersCSV("ComptesBancaires.csv",MethodeProjetBanque.COMPTES,6);
		do{
			if(connected){
				Menu.afficherMenu();
				saisie = Menu.saisieMenu(sc);
				if(saisie.equals("1")){
					MethodeProjetBanque.AGENCES = Ajouter.addTableau(MethodeProjetBanque.AGENCES,sc,3);
					GestionCSV.enregistrerAgenceCSV();
				}
				if(saisie.equals("2")){
					MethodeProjetBanque.CLIENTS = Ajouter.addTableau(MethodeProjetBanque.CLIENTS,sc,5);
					GestionCSV.enregistrerClientCSV();
				}
				if(saisie.equals("3")){
					MethodeProjetBanque.COMPTES = Ajouter.addTableau(MethodeProjetBanque.COMPTES,sc,6);
					GestionCSV.enregistrerCompteBancaireCSV();
				}
				if(saisie.equals("4")){
					MethodeProjetBanque.AGENCES = Modifier.modifierTableau(MethodeProjetBanque.AGENCES,sc,3);
					GestionCSV.enregistrerAgenceCSV();
				}
				if(saisie.equals("5")){
					MethodeProjetBanque.CLIENTS = Modifier.modifierTableau(MethodeProjetBanque.CLIENTS,sc,5);
					GestionCSV.enregistrerClientCSV();
				}
				if(saisie.equals("6")){
					MethodeProjetBanque.COMPTES = Modifier.modifierTableau(MethodeProjetBanque.COMPTES,sc,6);
					GestionCSV.enregistrerCompteBancaireCSV();
				}
				if(saisie.equals("7")){
					MethodeProjetBanque.AGENCES = Supprimer.supprimerTableau(MethodeProjetBanque.AGENCES,sc,3);
					GestionCSV.enregistrerAgenceCSV();
					GestionCSV.enregistrerCompteBancaireCSV();
				}
				if(saisie.equals("8")){
					MethodeProjetBanque.CLIENTS = Supprimer.supprimerTableau(MethodeProjetBanque.CLIENTS,sc,5);
					GestionCSV.enregistrerClientCSV();
					GestionCSV.enregistrerCompteBancaireCSV();
				}
				if(saisie.equals("9")){
					MethodeProjetBanque.COMPTES = Supprimer.supprimerTableau(MethodeProjetBanque.COMPTES,sc,6);
					GestionCSV.enregistrerCompteBancaireCSV();
				}
				if(saisie.equals("10")){
					MethodeProjetBanque.afficherTableau(MethodeProjetBanque.AGENCES);
				}
				if(saisie.equals("11")){
					MethodeProjetBanque.afficherTableau(MethodeProjetBanque.CLIENTS);
				}
				if(saisie.equals("12")){
					MethodeProjetBanque.afficherTableau(MethodeProjetBanque.COMPTES);
				}
				if(saisie.equals("13")){
					message = "Saisir le numéro de compte souhaité : ";
					MethodeProjetBanque.afficherByIndex(MethodeProjetBanque.COMPTES,message,sc,0);
				}
				if(saisie.equals("14")){
					message = "Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : ";
					MethodeProjetBanque.afficherByIndex(MethodeProjetBanque.CLIENTS,message,sc,0);
				}
				if(saisie.equals("15")){
					message = "Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : ";
					MethodeProjetBanque.afficherByIndex(MethodeProjetBanque.COMPTES,message,sc,2);
				}
				if(saisie.equals("16")){
					MethodeProjetBanque.afficherFicheClient(MethodeProjetBanque.CLIENTS,MethodeProjetBanque.COMPTES);
				}
				if(saisie.equals("17")){
					Menu.quitterProgramme(sc);
				}
			}
		}while(true);
	}
}