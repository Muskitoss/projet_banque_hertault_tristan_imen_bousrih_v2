import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class MethodeProjetBanque{
	public static String[][] AGENCES ={
		{"099","Agence Générique","5 rue du Lille à Lille"}
	};
	public static String[][] CLIENTS = new String [0][5];
	public static String[][] COMPTES = new String [0][6];
	public static void afficherTableau(String[] elements){
		if(elements.length==0){
			System.out.println("Erreur, vous n'avez pas créer le tableau que vous souhaitez afficher");
		}else{
			for (int i=0 ; i<elements.length; i++){		
				System.out.print(elements[i]+"\t");
			}
			System.out.println();
		}
	}
	public static void afficherTableau(String[][] elements){
		if(elements.length==0||elements[0].length==0){
			System.out.println("Erreur, tableau non existant !");
		}else{
			for (int i=0 ; i<elements.length; i++){
				afficherTableau(elements[i]);
			}
		}
	}
	public static boolean exist(String[] tableau,String saisie){
		boolean exist = false;
		for(int i=0;i<tableau.length;i++){
			if(tableau[i].equals(saisie)){
				exist = true;
			}
		}
		return exist;
	}
	public static boolean exist(String[][] tableau,String saisie){
		boolean exist = false;
		for(int i=0;i<tableau.length;i++){
			exist = exist(tableau[i],saisie);
			if(exist == true){
				return exist;
			}
		}
		return exist;
	}
	public static String saisieTableauVerifie(Scanner sc, String regexParam,String message){
		String saisie;
		boolean regex;
		do{
			System.out.println(message);
			saisie = sc.nextLine().trim();
			regex = regexMethode(saisie,regexParam);
			if(!regex){
				System.out.println("Erreur, saisie invalide !");
			}
		}while(!regex);
		return saisie;
	}
	public static String saisieTableauVerifie(Scanner sc, String regexParam,String message,String[][] tableau,boolean exist){
		String saisie;
		boolean regex;
		boolean saisieExist;
		do{
			System.out.println(message);
			saisie = sc.nextLine().trim();
			regex = regexMethode(saisie,regexParam);
			if(!regex){
				System.out.println("Erreur, saisie invalide !");
			}else{
				saisieExist = exist(tableau,saisie);
				if(saisieExist!=exist&&exist==false){
					System.out.println("Erreur, saisie déjà existant.");
					regex = false;
				}
				if(saisieExist!=exist&&exist==true){
					System.out.println("Erreur, saisie introuvable.");
					regex = false;
				}
			}
		}while(!regex);
		return saisie;
	}
	public static boolean regexMethode(String saisie, String regexParam){
		boolean regex = saisie.matches(regexParam);
		return regex;
	}
	public static String ageVerifie(Scanner sc,String message){
		String ageString;
		int jour;
		int mois;
		int annee;
		boolean ageIsValide = true;
		do{
			ageString = saisieTableauVerifie(sc,"\\d{8}",message);
			jour = Integer.parseInt(ageString.substring(0,2));
			mois = Integer.parseInt(ageString.substring(2,4));
			annee = Integer.parseInt(ageString.substring(4,8));
			if(jour>0){
				if((annee%100!=0)&&((annee%400==0)||(annee%4==0))){
					if(mois==1||mois==3||mois==5||mois==7
					||mois==8||mois==10||mois==12){
						if(jour>31){
							ageIsValide = false;
						}
					} else if(mois==4||mois==6
					||mois==9||mois==11){
						if(jour>30){
							ageIsValide = false;
						}
					} else if(mois==2){
						if(jour>29){
							ageIsValide = false;
						}
					} else {
						ageIsValide = false;
					}
				} else {
					if(mois==1||mois==3||mois==5||mois==7
					||mois==8||mois==10||mois==12){
						if(jour>31){
							ageIsValide = false;
						}
					} else if(mois==4||mois==6
					||mois==9||mois==11){
						if(jour>30){
							ageIsValide = false;
						}
					} else if(mois==2){
						if(jour>28){
							ageIsValide = false;
						}
					} else {
						ageIsValide = false;
					}
				}
			} else {
				ageIsValide = false;
			}
			if(ageIsValide){
				String jourString = String.valueOf(jour);
				String moisString = String.valueOf(mois);
				String anneeString = String.valueOf(annee);
				try{
					String dateAVerifier = jourString+"/"+moisString+"/"+anneeString;
					Date dateAComparer = new SimpleDateFormat("dd/MM/yyyy").parse(dateAVerifier);
					
					LocalDate dateVerifiee = dateAComparer.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					LocalDate dateDuJour = LocalDate.now();
					if(dateVerifiee.isAfter(dateDuJour)){
						System.out.println("Erreur, le client doit être née pour pouvoir créer un compte en son nom !!!");
						ageIsValide = false;
					}
				}catch(ParseException pe){
					System.out.println("Erreur format");
				}
			}
		}while(!ageIsValide);
		return ageString;
	}
	public static String typeCompteVerifie(Scanner sc,String message){
		String saisie;
		boolean saisieIsTrue = false;
		do{
			saisie = saisieTableauVerifie(sc,"[a-zA-Z]{1}",message).toLowerCase().trim();
			if(saisie.equals("c")){
				saisie = "Compte courrant";
				saisieIsTrue = true;
			}
			if(saisie.equals("a")){
				saisie = "Livret A";
				saisieIsTrue = true;
			}
			if(saisie.equals("e")){
				saisie = "Plan d'épargne logement";
				saisieIsTrue = true;
			}
		}while(!saisieIsTrue);
		return saisie;
	}
	public static String optionDecouvertAutorise(Scanner sc,String message){
		String saisie;
		boolean saisieIsTrue = false;
		do{
			saisie = saisieTableauVerifie(sc,"[a-zA-Z]{1}",message).toLowerCase().trim();
			if(saisie.equals("o")){
				saisie = "autorisé";
				saisieIsTrue = true;
			}
			if(saisie.equals("n")){
				saisie = "reffusé";
				saisieIsTrue = true;
			}
		}while(!saisieIsTrue);
		return saisie;
	}
	public static String[][] ajouterAncienTableau(String[][] tableau,int taille){
		String[][] nouveauTableau = new String[tableau.length+1][taille];
		if(tableau.length>0){
			for(int i=0;i<tableau.length;i++){
				nouveauTableau[i] = tableau[i];
			}
		}
		return nouveauTableau;
	}
	public static String[][] modifierElementTableau(String[][] tableau,String index,String ancienElement,String nouvelElement){
		String[][] nouveauTableau = new String[tableau.length][tableau[0].length];
		if(tableau.length>0){
			for(int i=0;i<tableau.length;i++){
				if(tableau[i][0].equals(index)){
					for(int j=1;j<tableau[i].length;j++){
						if(tableau[i][j].equals(ancienElement)){
							nouveauTableau[i][j] = nouvelElement;
							return nouveauTableau;
						}
					}
				}
			}
		}
		return nouveauTableau;
	}
	public static String saisieIndexValide(String[][] tableau,String message,Scanner sc){
		String saisie;
		boolean saisieModifieIsValide;
		do{
			System.out.println(message);
			saisie = sc.nextLine().trim();
			saisieModifieIsValide = regexMethode(saisie,"\\d");
			if(!saisieModifieIsValide){
				System.out.println("Erreur, saisie invalide");
			}else{
				if(Integer.parseInt(saisie)>tableau[0].length-1||Integer.parseInt(saisie)<1){
					System.out.println("Erreur, l'index saisie n'est pas valide");
					saisieModifieIsValide = false;
				}else{
					for(int i=1;i<tableau[0].length;i++){
						if(Integer.parseInt(saisie)==i){
							saisieModifieIsValide = true;
							break;
						}
					}
				}
			}
		}while(!saisieModifieIsValide);
		return saisie;
	}
	public static String saisieElementValide(String[][] tableau,String indexSaisie,String message,Scanner sc){
		String saisie = "";
		if(tableau[0].length==3){
			if(Integer.parseInt(indexSaisie)==1){
				message = "Saisir le nom de l'agence : ";
				saisie = saisieTableauVerifie(sc,"[a-zA-Z\\-\\s]{3,50}",message);
				saisie = saisie.substring(0,1).toUpperCase()+saisie.substring(1).toLowerCase();
			}
			if(Integer.parseInt(indexSaisie)==2){
				message = "Saisir l'adresse de l'agence : ";
				saisie = saisieTableauVerifie(sc,"[\\w\\s]{5,150}",message);
			}
		}
		if(tableau[0].length==5){
			if(Integer.parseInt(indexSaisie)==1){
				message = "Saisir le nom du client : ";
				saisie = saisieTableauVerifie(sc,"[a-zA-Z]{3,30}",message).toUpperCase();
			}
			if(Integer.parseInt(indexSaisie)==2){
				message = "Saisir le prénom du client : ";
				saisie = saisieTableauVerifie(sc,"[a-zA-Z]{1,20}[a-zA-Z\\-]{0,1}[a-zA-Z]{1,20}",message);
				saisie = saisie.substring(0,1).toUpperCase()+saisie.substring(1).toLowerCase();
			}
			if(Integer.parseInt(indexSaisie)==3){
				message = "Saisir la date de naissance du client (jjmmaaaa) : ";
				saisie = ageVerifie(sc,message);
			}
			if(Integer.parseInt(indexSaisie)==4){
				message = "Saisir l'adresse email du client : ";
				saisie = saisieTableauVerifie(sc,"\\w{3,20}\\.{0,1}\\w{3,20}\\@\\w{2,15}\\.[a-z]{2,7}",message);
			}
		}
		if(tableau[0].length==6){
			if(Integer.parseInt(indexSaisie)==1){
				message = "Saisir le code agence (il doit se composer de trois chiffres) : ";
				saisie = saisieTableauVerifie(sc,"\\d{3}",message,AGENCES,true);
			}
			if(Integer.parseInt(indexSaisie)==2){
				message = "Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : )";
				saisie = saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,CLIENTS,true).toUpperCase();
			}
			if(Integer.parseInt(indexSaisie)==3){
				message = "Saisir la touche associée aux types de compte bancaire parmis\ncompte courrant : \"c\"\nlivret A : \"a\"\nplan d'épargne logement : \"e\"";
				saisie = typeCompteVerifie(sc,message);
			}
			if(Integer.parseInt(indexSaisie)==4){
				message = "Saisir le solde du compte bancaire : ";
				saisie = saisieTableauVerifie(sc,"\\d{1,20}",message);
			}
			if(Integer.parseInt(indexSaisie)==5){
				message = "Saisir les options du découvert : \"o\" pour autorisé\n\"n\" pour reffusé";
				saisie = optionDecouvertAutorise(sc,message);
			}
		}
		return saisie;
	}
	public static String[][] tableauModifie(String[][] tableau,String idSaisie, String indexSaisie,String nouvelElement,int index){
		String[][] tableauModifie = tableau;
		for(int i=0;i<tableau.length;i++){
			if(tableau[i][index].equals(idSaisie)){
				tableauModifie[i][Integer.parseInt(indexSaisie)] = nouvelElement;
			}
		}
		return tableauModifie;
	}
	public static String[][] tableauModifie(String[][] tableau,String idSaisie, String indexSaisie,String nouvelElement){
		String[][] tableauModifie = tableauModifie(tableau,idSaisie,indexSaisie,nouvelElement,0);
		return tableauModifie;
	}
	public static String[][] tableauSupprime(String[][] ancienTableau,String codeElement,int index){
		int cpt = 0;
		for(int i=0;i<ancienTableau.length;i++){
			if(ancienTableau[i][index].equals(codeElement)){
				cpt++;
			}
		}
		String[][] nouveauTableau = new String[ancienTableau.length-cpt][ancienTableau[0].length];
		cpt = 0;
		for(int i=0;i<nouveauTableau.length;i++){
			if(ancienTableau[i][index].equals(codeElement)){
				cpt++;
			}
			nouveauTableau[i] = ancienTableau[i+cpt];
		}
		return nouveauTableau;
	}
	public static void afficherByIndex(String[][] tableau,String message,Scanner sc,int index){
		String numeroCompte = "";
		if(tableau.length==0||tableau[0].length==0){
			System.out.println("Erreur, il n'y a pas de tableau !");
		}else{
			if(message.equals("Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : ")){
				numeroCompte = saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,tableau,true).toUpperCase();
			}
			if(message.equals("Saisir le numéro de compte souhaité : ")){
				numeroCompte = saisieTableauVerifie(sc,"\\d{11}",message,tableau,true);
			}
			for(int i=0;i<tableau.length;i++){
				if(tableau[i][index].equals(numeroCompte)){
					afficherTableau(tableau[i]);
				}
			}
		}
	}
	public static void afficherFicheClient( String [][] tableauClient ,  String [][] tableauCompteBancaire){
		if(tableauClient.length==0||tableauClient[0].length==0
		||tableauCompteBancaire.length==0||tableauCompteBancaire[0].length==0){
			System.out.println("Erreur, on ne peux pas imprimer des tableaux si ils n'existent pas !!!");
		}else{
			Scanner sc = new Scanner(System.in);
			int taille =-1;
			String message = "Saisir l'id du client (il doit se composer de deux lettres en majuscules et de six chiffres) : ";
			String idClient = saisieTableauVerifie(sc,"[a-zA-Z]{2}\\d{6}",message,tableauClient,true);
			for( int i = 0 ; i< tableauClient.length;i++){
				if (tableauClient[i][0].equals (idClient)){
					taille = i;
				}
			}	
			int indexTaille = 0;
			for( int i = 0 ; i< tableauCompteBancaire.length;i++){
				if (tableauCompteBancaire[i][2].equals (idClient)){
					indexTaille++;
				}
			}
			String [][] indexTableau = new String [indexTaille][6];
			for (int i=0 ; i<tableauCompteBancaire.length ; i++){
				if(tableauCompteBancaire[i][2].equals (idClient)){
					indexTableau[i]= tableauCompteBancaire[i];
				}
			}
			String content = "";
			try{
				File file = new File("ficheClient.txt");
				
				if (!file.exists()){
					file.createNewFile();
				}
				content= "\t\t\t Fiche Client\nNumero Client: ";
				content+=idClient;
				content+="\nnom: ";
				content+=tableauClient[taille][1];
				content+="\nprenom: ";
				content+=tableauClient[taille][2];
				content+="\n Date de naissance: ";
				content+=tableauClient[taille][3];
				content+="\n________________________________________________\n Liste Compte:";
				content+="\n________________________________________________\n Numero du Compte";
				content+="\t\t\t Solde  \n________________________________________________\n";
				for(int i =0 ; i <indexTableau.length ; i++){
					
					content += indexTableau[i][0];
					content +="\t\t\t";
					content +=indexTableau[i][4];
					if(Integer.parseInt(indexTableau[i][4])<0 ){
						content += "\t\t\t:( \n";
					}else{
						content += "\t\t\t:)\n";
					}
				}
				FileWriter fw =new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
			} catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}