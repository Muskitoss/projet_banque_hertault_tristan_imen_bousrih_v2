import java.util.*;
import java.io.*;
import java.text.*;
import java.time.*;

public class GestionCSV{
	private static final String DELIMITER = ",";
	private static final String SEPARATOR = "\n";
    public static void enregistrerAgenceCSV()
    {
		//En-tête de fichier
		String Header = "code agence,nom agence,adresse agence";
		FileWriter file = null;
		
		try
		{
			file = new FileWriter("Agences.csv");
			//Ajouter l'en-tête
			file.append(Header);
			//Ajouter une nouvelle ligne après l'en-tête
			file.append(SEPARATOR);
			for(int i=0;i<MethodeProjetBanque.AGENCES.length;i++){
				file.append(MethodeProjetBanque.AGENCES[i][0]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.AGENCES[i][1]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.AGENCES[i][2]);
				file.append(SEPARATOR);
			}
			file.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
	public static void enregistrerClientCSV()
    {
		//En-tête de fichier
		String Header = "id client,nom client,pénom client,date de naissance,adresse mail";
		FileWriter file = null;
		
		try
		{
			file = new FileWriter("Clients.csv");
			//Ajouter l'en-tête
			file.append(Header);
			//Ajouter une nouvelle ligne après l'en-tête
			file.append(SEPARATOR);
			for(int i=0;i<MethodeProjetBanque.CLIENTS.length;i++){
				file.append(MethodeProjetBanque.CLIENTS[i][0]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.CLIENTS[i][1]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.CLIENTS[i][2]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.CLIENTS[i][3]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.CLIENTS[i][4]);
				file.append(SEPARATOR);
			}
			file.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
	public static void enregistrerCompteBancaireCSV()
    {
		//En-tête de fichier
		String Header = "numéro de compte,code agence,id client,type de compte,solde bancaire,options de découvert";
		FileWriter file = null;
		
		try
		{
			file = new FileWriter("ComptesBancaires.csv");
			//Ajouter l'en-tête
			file.append(Header);
			//Ajouter une nouvelle ligne après l'en-tête
			file.append(SEPARATOR);
			for(int i=0;i<MethodeProjetBanque.COMPTES.length;i++){
				file.append(MethodeProjetBanque.COMPTES[i][0]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.COMPTES[i][1]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.COMPTES[i][2]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.COMPTES[i][3]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.COMPTES[i][4]);
				file.append(DELIMITER);
				file.append(MethodeProjetBanque.COMPTES[i][5]);
				file.append(SEPARATOR);
			}
			file.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
	public static String[][] lectureFichiersCSV(String nomFichierCSV,String[][] tableau,int taille){
		try {
        File file = new File(nomFichierCSV);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = "";
		int cpt = 0;
		while((line = br.readLine()) != null){
			cpt++;
		}
		fr = new FileReader(file);
		br = new BufferedReader(fr);
		tableau = new String[cpt][taille];
		int i = 0;
        while((line = br.readLine()) != null){
				tableau[i] = line.split(DELIMITER);
				for(String test : tableau[i]){
					System.out.print(test + " ");
				}
            System.out.println();
			i++;
        }
        br.close();
        }catch(IOException ioe){
			ioe.printStackTrace();
        }
		return tableau;
	}
}